import React, { Component } from "react";
import Exemple from "./Exemple";
import YoutubePlayer from "../YoutubePlayer/YoutubePlayerContainer";

class ModalYoutupeContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }
  componentDidMount() {}
  render() {
    return (
      <div className="col-xs-12 center-xs">
        <YoutubePlayer height="720" width="1280" videoId={this.props.videoId} />
      </div>
    );
  }
}

export default ModalYoutupeContainer;
