import styled from "styled-components";

export default styled.div`
  h1  {
    font-weight: 900;
    font-size: 3rem;
  }
`;
