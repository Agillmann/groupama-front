import React, {Component}from "react";
import { Switch, Route } from "react-router-dom";

import Home from "./Pages/Home/HomeContainer";
import Episodes from "./Pages/Episodes/EpisodesContainer";
import Aventure from "./Pages/Aventure/AventureContainer";
import Contact from "./Pages/Formulaire/Contact";
import Exemple from "./ModalYoutube/ModalYoutubeContainer";
import Emission from "./Pages/Emission/EmissionContainer";

class Main extends Component {   
  render() {
    return (
      <main className="row">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/emission" component={Emission} />
          <Route path="/episodes" component={Episodes} />
          <Route path="/aventure" component={Aventure} />
          <Route path="/contact" component={Contact} />
          <Route path="/test" component={Exemple} />
        </Switch>
      </main>
    );
  }
}

export default Main;
