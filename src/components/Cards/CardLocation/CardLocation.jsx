import React from "react";
import Style from "./Style";

import styled from "styled-components";
const Vignette = styled.div`
    background: url(${props =>
      props.vignette ? props.vignette : "img/Pro.png"}) no-repeat center;
    background-size:cover;
    font-size: 5rem;
    text-align: center;
    padding: 100px 40px;
`;


const CardLocation = ({ville, description, cardImg, de}) => (
  <Style className="col-xs-12">
      {/* <Img src={cardImg} alt="photo" /> */}
      <Vignette vignette={cardImg}/>
    <h4 className="center-xs">
      L’émission {de} <span>{ville}</span>
    </h4>
    <hr style={{ width: "20%" }} />
    <p>{description}</p>
  </Style>
);

export default CardLocation;
