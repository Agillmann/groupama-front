import React, { Component } from "react";
import CardLocation from "./CardLocation";

class CardLocationContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ville: "",
      description:"",
      cardImg: "",
      data: []
    };
  }
  componentDidMount() {
    this.setState({ville: this.props.ville, description: this.props.description, cardImg: this.props.cardImg})
  }
  render() {
    return (
      <div className="row">
        <CardLocation 
          ville={this.state.ville}
          description={this.state.description}
          cardImg={this.state.cardImg}
          de={this.props.de}
        />
      </div>
    );
  }
}

export default CardLocationContainer;
