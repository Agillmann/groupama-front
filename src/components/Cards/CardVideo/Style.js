import styled from "styled-components";
import { colors } from "../../../utils/styles/helpers";
const { orange, primary, grey, white } = colors;

export default styled.div`
  margin: 10px 0px 30px;
  padding: 0px 0px 30px 0px !important;
  cursor: pointer;
  border-radius: 10px;
  min-height: 4vh;
  background: ${white}
  -webkit-box-shadow: 0px 1px 8px 0.6px rgba(66, 66, 66, 1);
  -moz-box-shadow: 0px 1px 8px 0.6px rgba(66, 66, 66, 1);
  box-shadow: 0px 0px 2px 0.6px rgba(66, 66, 66, 1);

  & > h4 {
    font-family: "Open Sans", sans-serif;
    font-weight: 500;
    letter-spacing: 0.1em;
    font-size: 1rem;
    margin: 15px 0px 0px 0px;
    color: ${primary};
    & > span {
      font-family: "Satisfy", cursive;
      font-weight: 400;
      font-size: 1.5rem;
      color: ${orange};
    }
  }
  & > p {
    font-family: "Open Sans", sans-serif;
    font-weight: 500;
    font-size: 1rem;
    color: ${grey};
    text-align: center;
    padding-left: 18px;
    padding-right: 18px;
  }
`;
