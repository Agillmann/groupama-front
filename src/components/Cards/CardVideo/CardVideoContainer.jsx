import React, { Component } from "react";
import CardVideo from "./CardVideo";

class CardVideoContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      title: "Épisode 1",
      subTitle: "Title de la video faer azdazdazd z adzaz dazd a",
      vignette: ""
    };
  }

  componentDidMount() {
    this.setState({
      vignette: this.props.vignette,
      subTitle: this.props.subTitle,
      title: this.props.title
    });
  }
  render() {
    return (
      <div className="row start-md">
        <CardVideo
          vignette={this.props.vignette}
          subTitle={this.props.subTitle}
          title={this.props.title}
        />
      </div>
    );
  }
}

export default CardVideoContainer;
