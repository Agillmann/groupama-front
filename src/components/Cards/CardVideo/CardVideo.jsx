import React from "react";
import Style from "./Style";

import styled from "styled-components";
import { colors } from "../../../utils/styles/helpers";
const { orange, white } = colors;
const Vignette = styled.div`
    background: linear-gradient(rgba(0,0,0,0.6), rgba(0,0,0,0.6)),  url(${props =>
      props.vignette ? props.vignette : "img/Pro.png"}) no-repeat center;
    background-size:cover;
    font-size: 5rem;
    text-align: center;
    padding: 60px 40px;
    & i {
        border-radius:50%;  
        color:${orange}
        background:${white}
        transition: all 0.5s ease-out;  
        &:hover {
            box-shadow: 0 4px 9px rgba(0,0,0,0.3);
            position:relative;
            top: -2px;
          }  
    }
`;

const CardVideo = ({ vignette, title, subTitle }) => (
  <Style className="col-xs-12">
    <Vignette vignette={vignette}>
      <i className="fas fa-play-circle" />
    </Vignette>
    <h4 className="center-xs">
      <span>{title}</span> : {subTitle}
    </h4>
  </Style>
);

export default CardVideo;
