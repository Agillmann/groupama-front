import React, { Component } from 'react';
import YoutubePlayer from './YoutubePlayer';

class YoutubePlayerContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      opts: {
        height: this.props.height,
        width: this.props.width,
        playerVars: {
          // https://developers.google.com/youtube/player_parameters
          autoplay: 0,
        },
      },
    };
  }

  componentDidMount() {}

  handleClickCenter = (lng, lat, zoom) => {
    this.setState({ center: [lng, lat], zoom: [zoom] });
  };

  render() {
    return (
      <YoutubePlayer
        className="row"
        height={this.state.height}
        width={this.state.width}
        videoId={this.props.videoId}
        opts={this.state.opts}
      />
    );
  }
}

export default YoutubePlayerContainer;
