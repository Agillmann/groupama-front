import React from 'react';
import Youtube from 'react-youtube';

const YoutubePlayer = ({ videoId, opts }) => (
  <Youtube className="row" videoId={videoId} opts={opts} host="https://www.youtube.com" />
);

export default YoutubePlayer;
