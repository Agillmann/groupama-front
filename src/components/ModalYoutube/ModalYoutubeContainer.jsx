import React, { Component } from "react";
import CardVideo from "../Cards/CardVideo/CardVideoContainer";
import YoutubePlayer from "../YoutubePlayer/YoutubePlayerContainer";
import { Modal, Container, Col, Row } from "react-bootstrap";
import styled from "styled-components";
import { colors } from "../../utils/styles/helpers";
import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  EmailShareButton
} from "react-share";
import {
  FacebookIcon,
  TwitterIcon,
  LinkedinIcon,
  EmailIcon
} from "react-share";
const { orange, grey, primary } = colors;
const Title = styled.h3`
  font-family: "Satisfy", cursive !important;
  font-weight: 400 !important;
  font-size: 3rem !important;
  color: ${orange};
  & > span {
    font-size: 2rem !important;
    color: ${primary};
  }
`;

const Text = styled.p`
    font-family: 'Open Sans', sans-serif;
    font-weight: 300;
    font-style:italic
    letter-spacing: 0.1em;
    font-size: 0.5em;
    margin: 0;
    color:${grey}
    `;
const IconClose = styled.i`
  font-size: 2.5em;
  cursor: pointer;
  &:hover {
    color: ${orange};
  }
`;

class ModalYoutubeContainer extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);

    this.state = {
      show: false,
      title: "Épisode 1",
      subTitle: "Title de la video faer azdazdazd z adzaz dazd a",
      videoId: null,
      videoUrl: "https://www.youtube.com/watch?v=",
      vignette: "img/Pro.png"
    };
  }

  componentDidMount() {
    this.setState({
      videoId: this.props.videoId,
      videoUrl: this.state.videoUrl + this.props.videoId,
      subTitle: this.props.subTitle,
      title: this.props.title,
      vignette: this.props.vignette
    });
  }
  handleClose() {
    this.setState({ show: false });
  }

  handleShow() {
    this.setState({ show: true });
  }

  render() {
    //console.log(this.state.videoUrl );
    return (
      <div className="col-xs-12">
        <div onClick={this.handleShow}>
          <CardVideo
            title={this.state.title}
            subTitle={this.props.subTitle}
            vignette={this.props.vignette}
          />
        </div>
        <Modal
          show={this.state.show}
          onHide={this.handleHide}
          dialogClassName="modal-100w"
          aria-labelledby="contained-modal-title-vcenter"
        >
          <Modal.Header>
            <Modal.Title id="contained-modal-title-vcenter">
              <div className="row bottom-xs" style={{ marginBottom: 10 }}>
                <Title className="col-xs-12 start-xs">
                  {this.state.title}
                  <span> : {this.state.subTitle}</span>
                </Title>

                <div className="col-md-3 col-sm-4 col-xs-5 start-xs">
                  <Text>Partager la video : </Text>
                </div>
                <div className="col-xs-1">
                  <FacebookShareButton
                    url={this.state.videoUrl}
                    quote={this.state.title}
                    className=""
                    style={{ cursor: "pointer" }}
                  >
                    <FacebookIcon size={32} round />
                  </FacebookShareButton>
                </div>
                <div className="col-xs-1">
                  <TwitterShareButton
                    url={this.state.videoUrl}
                    quote={this.state.title}
                    style={{ cursor: "pointer" }}
                    className=" "
                  >
                    <TwitterIcon size={32} round />
                  </TwitterShareButton>
                </div>
                <div className="col-xs-1">
                  <LinkedinShareButton
                    url={this.state.videoUrl}
                    quote={this.state.title}
                    style={{ cursor: "pointer" }}
                    className=" "
                  >
                    <LinkedinIcon size={32} round />
                  </LinkedinShareButton>
                </div>
                <div className="col-xs-2">
                  <EmailShareButton
                    url={this.state.videoUrl}
                    quote={this.state.title + " " + this.s}
                    style={{ cursor: "pointer" }}
                    className=" "
                  >
                    <EmailIcon size={32} round />
                  </EmailShareButton>
                </div>
              </div>
            </Modal.Title>
            <IconClose className="fas fa-times" onClick={this.handleClose} />
          </Modal.Header>
          <Modal.Body>
            <Container fluid={true}>
              <Row className="show-grid">
                <Col
                  xs={12}
                  md={12}
                  style={{ paddingRight: 0, paddingLeft: 0 }}
                >
                  <YoutubePlayer
                    height="720"
                    width="100%"
                    videoId={this.state.videoId}
                  />
                </Col>
              </Row>
            </Container>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
export default ModalYoutubeContainer;
