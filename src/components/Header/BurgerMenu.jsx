import React, { Component } from "react";
import { Link } from "react-router-dom";
import { slide as Menu } from "react-burger-menu";

class BurgerMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataCli: []
    };
  }
  render() {
    return (
      <Menu slide right width={"95%"}>
        <Link id="about" className="menu-item" to="/emission">
          L'émission
        </Link>
        <Link id="contact" className="menu-item" to="/episodes">
          Les épisodes
        </Link>
        <Link id="contact" className="menu-item" to="/aventure">
          L'Aventure
        </Link>
        <hr style={{ border: "0.5px solid greylight" }} />
        <Link id="contact" className="menu-item" to="/contact">
          Contact
        </Link>
        <a
          className="menu-item"
          href="https://www.groupama.fr/mentions-legales"
          style={{ textDecoration: "none" }}
          target="blank"
        >
          Mention légales
        </a>
        <a
          className="menu-item"
          href="https://www.groupama.fr/donnees-personnelles"
          style={{ textDecoration: "none" }}
          target="blank"
        >
          Données Personnelles
        </a>
      </Menu>
    );
  }
}
export default BurgerMenu;
