import React from "react";
import { Link } from "react-router-dom";

import styled from "styled-components";
import { colors } from "../../utils/styles/helpers";
import BurgerMenu from "./BurgerMenu";

const { primary, white, black, grey } = colors;

const MiddleHeader = styled.header`
  position:fixed;
  width: 100%;
  padding 20px 0;
  z-index:99;
  background-color: #FFFFFF;
  border-bottom: 0.25rem solid ${primary};
  transition: all 0.5s ease-out;
  .image-content {
    @media (max-width: 1023px){}
  }
  .LinkRoute {
    @media (max-width: 1023px){
      display: none!important;
    }
  }
`;
const DropdownContent = styled.div`
  position: absolute;
  left: 35px;
  background-color: ${white};
  min-width: 250px;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
  z-index: 1;
  visibility: hidden;
  opacity: 0;
  transition: visibility 0s, opacity 0.2s linear;
  & > a {
    color: ${black};
    padding: 40px;
    text-decoration: none;
  }
  @media (max-width: 1023px) {
    display: none!important;
  }
`;

const Dropdown = styled.div`
  position: relative;
  &:hover .dropdown-content {
    display: block;
    visibility: visible;
    opacity: 1;
  }
  @media (max-width: 1023px) {
    display: none!important;
  }
`;
const TextLink = styled.h2`
  color: ${grey};
  font-family: "Satisfy", cursive;
  font-weight: 400;
  font-size: 1.5rem;
  @media (max-width: 1023px) {
    display: none!important;
  }
`;
const TextLinkD = styled.h2`
  color: ${grey};
  font-family: "Satisfy", cursive;
  font-weight: 400;
  padding: 20px 0;
  font-size: 1.5em;
`;
const Img = styled.img`
  max-width: 300px;
  height: auto;
  transition: all 0.5s ease-out;
  @media (max-width: 756px){
    max-width: 200px;
  }
`;

const Header = () => (
  <MiddleHeader className="row">
    <div className="col-xs-12">
      <div className="row middle-md start-sm">
      <Link
      className="LinkRoute col-sm-2 center-sm"
      to="/emission"
      style={{ textDecoration: "none" }}
    >
      <TextLink>L'émission</TextLink>
    </Link>
    <Link
      className="LinkRoute col-sm-2 center-sm"
      to="/episodes"
      style={{ textDecoration: "none" }}
    >
      <TextLink>Les épisodes</TextLink>
    </Link>
    <Link
      className="image-content col-sm-4 center-md start-sm"
      to="/"
      style={{ textDecoration: "none" }}
    >
      <Img className="logo" src="img/logo.jpeg" alt="Logo" />
    </Link>
    <Link
      className="LinkRoute col-sm-1 center-sm"
      to="/aventure"
      style={{ textDecoration: "none" }}
    >
      <TextLink>L'Aventure</TextLink>
    </Link>
    <Dropdown className="col-sm-3 center-sm">
      <TextLink>Espace Presse</TextLink>

      <DropdownContent className="dropdown-content">
        {/* <Link to="/kit-presse" style={{ textDecoration: "none", padding: 0 }}>
          <TextLinkD className="center-sm">Kit de presse</TextLinkD>
        </Link> */}
        <Link to="/contact" style={{ textDecoration: "none", padding: 0 }}>
          <TextLinkD className="center-sm">Contactez-nous</TextLinkD>
        </Link>
      </DropdownContent>
    </Dropdown>
    <BurgerMenu></BurgerMenu>
      </div>
    </div>
    
  </MiddleHeader>
);

export default Header;
