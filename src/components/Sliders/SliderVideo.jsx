import React, { Component } from "react";
import SlickSlider from "react-slick";
import ModalYoutube from "../ModalYoutube/ModalYoutubeContainer";
import api from "../../utils/api/api";
class SliderVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      limit: false,
      data: [],
      lireSuite: false
    };
  }
  async componentDidMount() {
    const fetchData = await api.getVideoList();
    await this.setState({ data: fetchData, limit: this.props.limit });
  }
  splitId = str => {
    return str.split("watch?v=")[1];
  };
  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      autoplay: true,
      autoplaySpeed: 4000,
      pauseOnHover: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 765,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    return (
      <div className="col-xs-12">
        <SlickSlider {...settings}>
          {this.state.limit && this.state.data.length > 0
            ? this.state.data
                .slice(0, 3)
                .map(video => (
                  <ModalYoutube
                    title={video.title}
                    subTitle={video.subTitle}
                    videoId={this.splitId(video.videoUrl)}
                    vignette={
                      video.vignette
                        ? video.vignette.url
                        : "img/video-vignette-test.png"
                    }
                    key={this.splitId(video.videoUrl)}
                  />
                ))
            : this.state.data.map(video => (
                <ModalYoutube
                  title={video.title}
                  subTitle={video.subTitle}
                  videoId={this.splitId(video.videoUrl)}
                  vignette={
                    video.vignette
                      ? video.vignette.url
                      : "img/video-vignette-test.png"
                  }
                  key={this.splitId(video.videoUrl)}
                />
              ))}
        </SlickSlider>
      </div>
    );
  }
}

export default SliderVideo;
