import React, { Component } from "react";
import SlickSlider from "react-slick";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Img = styled.img`
  width: 100%;
  height: ${props => props.imgHeight};
  margin: 0px !important;
  padding:0px!important;

`;

const ImgContent = styled.div``;
class Slider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataJson: [],
      lireSuite: false
    };
  }
  async componentDidMount() {
    await this.setState({ data: this.props.dataJson });
  }
  render() {
    const settings = {
      dots: false,
      arrows: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 10000,
      pauseOnHover: false,
      cssEase: "linear"
    };
    const dataSlide = require(`./${this.props.dataJson}`);
    //const photos = this.props.data;
    const photos = dataSlide.photos;
    return (
      <div className="col-xs-12" style={{ paddingLeft: 0, paddingRight: 0 }}>
        <SlickSlider {...settings}>
          {photos.length > 0 &&
            photos.map(p => (
              <div className="row" key={p.id}>
                <ImgContent
                  className="col-xs-12"
                  style={{ paddingLeft: 0, paddingRight: 0 }}
                  
                >
                  <Link to={p.url_link}>
                    <Img src={p.img_src} alt={p.img_alt} imgHeight={this.props.imgHeight} key={p.id} />
                    
                  </Link>
                </ImgContent>
              </div>
            ))}
        </SlickSlider>
      </div>
    );
  }
}

export default Slider;
