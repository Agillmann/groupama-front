import React, { Component } from "react";
import SlickSlider from "react-slick";
import ModalYoutube from "../ModalYoutube/ModalYoutubeContainer";
import api from "../../utils/api/api";
class SliderVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      limit: false,
      data: [],
      lireSuite: false,
      nbVideo: 0,
      isFetching: false
    };
  }
  async componentDidMount() {
    const fetchData = await api.getBestVideo();
    await this.setState({
      data: fetchData,
      limit: this.props.limit,
      isFetching: fetchData.videos.length > 0 ? true : false,
      nbVideo: fetchData.videos.length
    });
  }
  splitId = str => {
    return str.split("watch?v=")[1];
  };
  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToScroll: 3,
      autoplay: true,
      autoplaySpeed: 4000,
      pauseOnHover: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 765,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    // console.log("data", this.state.data);
    return (
      <div className="col-xs-12">
        {this.state.isFetching ? (
          <SlickSlider
            {...settings}
            slidesToShow={this.state.nbVideo <= 3 ? this.state.nbVideo : 3}
          >
            {this.state.data.videos.map(video => (
              <ModalYoutube
                title={video.title}
                subTitle={video.subTitle}
                videoId={this.splitId(video.videoUrl)}
                vignette={
                  video.vignette
                    ? video.vignette.url
                    : "img/video-vignette-test.png"
                }
                key={this.splitId(video.videoUrl)}
              />
            ))}
          </SlickSlider>
        ) : (
          <h3
            className="center-xs"
            style={{ fontSize: "3rem", margin: "100px 0px 100px 0px" }}
          >
            Vidéos bientôt disponibles
          </h3>
        )}
      </div>
    );
  }
}

export default SliderVideo;
