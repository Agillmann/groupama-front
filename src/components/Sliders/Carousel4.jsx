import React from "react";
import { Carousel } from "react-responsive-carousel";
// import { Link } from "react-router-dom";

export default () => (
  <Carousel
    autoPlay
    showArrows={false}
    showIndicators={false}
    showThumbs={false}
    showStatus={false}
    infiniteLoop
    stopOnHover={false}
    interval={5000}
  >
    <img src="img/main-img-episodes.jpeg" alt="illustration velo"/>
  </Carousel>
);
