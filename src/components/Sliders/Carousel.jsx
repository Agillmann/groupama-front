import React from "react";
import { Carousel } from "react-responsive-carousel";
import { Link } from "react-router-dom";

export default () => (
  <Carousel
    autoPlay
    showArrows={false}
    showIndicators={false}
    showThumbs={false}
    showStatus={false}
    infiniteLoop
    stopOnHover={false}
    interval={5000}
    dynamicHeight={true}
  >
    <Link to="/">
      <img src="img/main-carrousel.jpeg" alt="illustration velo"/>
    </Link>
    <Link to="/aventure">
      <img src="img/Picture2.png" alt="illustration velo"/>
    </Link>
    <Link to="/episodes">
      <img src="img/Picture3.png" alt="illustration velo"/>
    </Link>
    <Link to="/episodes">
      <img src="img/Picture4.png" alt="illustration velo"/>
    </Link>
  </Carousel>
);
