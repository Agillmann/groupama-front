import React, { Component } from "react";
import SlickSlider from "react-slick";
import CardLocation from "../Cards/CardLocation/CardLocationContainer";

class SliderLocal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      limit: false,
      data: [],
      lireSuite: false
    };
  }
  async componentDidMount() {}
  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 2,
      autoplay: true,
      autoplaySpeed: 4000,
      pauseOnHover: true,
      customPaging: () => {
        return (
          <a>test</a>
        );
      },
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 765,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    return (
      <div className="col-xs-12">
        <SlickSlider {...settings}>
        <div
             
            >
              <CardLocation
                ville="Bagnères-de-Bigorre"
                description="Le 5 Juin devant l'hôtel Carré Py à partir de 18H"
                cardImg="img/Le-geant-du-Tourmalet_format_894x500.jpg"
              />
            </div>
            <div
              className="col-md-6"
              
            >
              <CardLocation
                ville="Gavarnie"
                description="Le 6 Juin dans le village de Gavarnie à partir de 9H du matin"
                cardImg="img/Gavarnie_recti_small_Wikimedia_Commons.jpg"
              />
            </div>
        </SlickSlider>
      </div>
    );
  }
}

export default SliderLocal;
