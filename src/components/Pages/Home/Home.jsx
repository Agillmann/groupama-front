import React from "react";
import Style from "./Style";
// import Slider from "../../Sliders/Slider";
import { Link } from "react-router-dom";
import MapBoxSection from "../../Mapbox/MapboxContainer";
import styled, { keyframes } from "styled-components";
import { colors } from "../../../utils/styles/helpers";

import { pulse, bounce } from "react-animations";
import Carousel from "../../Sliders/Carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
const pulseAnimation = keyframes`${pulse}`;
const bounceAnimation = keyframes`${bounce}`;

const PulseDiv = styled.div`
  &:hover {
    animation: 2s ${pulseAnimation};
  }
  & > span:hover {
    animation: 1s ${pulseAnimation};
  }
`;

const { orange, white, grey } = colors;

const ImgPart = styled.img`
  width: 250px;
  height: auto;
  @media (max-width: 756px) {
    width: 150px;
  }
`;

const ImgContent = styled.div`
  @media (max-width: 756px) {
    margin-bottom: 30px !important;
  }
`;

const Button = styled.button`
  font-family: "Open Sans", sans-serif;
  font-weight: 500;
  letter-spacing: 0.1em;
  font-size: 1.1em;
  color: ${white};
  border-radius: 34px;
  padding: 13px 23px;
  border: none;
  background: ${orange};
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15);
  transition: 0.33s;
  &:after {
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15);
  }
  /* Transition to showing the bigger shadow on hover */
  &:hover {
    box-shadow: 0 4px 9px rgba(0, 0, 0, 0.3);
    position: relative;
    top: -2px;
  }
  box-shadow: 8px 10px 8px -9px #777;
  cursor: pointer;
`;
const ChevronDown = styled.i`
  font-size: 3.5rem;
  color: ${grey};
  position: absolute;
  margin-left: auto;
  margin-right: auto;
  width: auto;
  left: 0;
  right: 0;
  top: 2em;
  font-size: 2rem;
  text-align: center;
  z-index: 3;
`;
const Scroll = styled.img`
  position: absolute;
  margin-left: auto;
  margin-right: auto;
  left: 0;
  right: 0;
  bottom: 25px;
  width: 56px;
  height: auto;
  color: ${orange};
  animation-duration: 3s;
  animation-delay: 2s;
  animation: 1s ${bounceAnimation};
  & path {
    fill: ${grey};
  }
`;
const CarouselContent = styled.div`
  padding: 0px !important;
  margin: 15px 0px 0px 0px !important;
  @media (max-width: 1023px) {
    margin-top: 65px !important;
  }
`;
const Home = ({ lireSuite, onClickSuite }) => (
  <Style className="row">
    <CarouselContent
      className="col-xs-12"
      style={{
        marginBottom: 0,
        marginTop: 0
      }}
    >
      {/* <Slider dataJson="imageData.json" imgHeight="100vh"/> */}
      <Carousel />
      <Scroll src="img/scroll.svg" alt="scroll" />
    </CarouselContent>

    {/* Section Concept */}
    <div
      className="col-xs-12"
      style={{
        textAlign: "center",
        height: "auto",
        paddingLeft: 0,
        paddingRight: 0
      }}
    >
      <div className="row">
        <div
          className="col-md-6 col-sm-12 paragraphe"
          style={{ paddingLeft: 0, paddingRight: 0 }}
        >
          <h2>Notre Concept</h2>
          <div className="row">
            <div className="col-md-offset-1 col-md-10">
                <div>
                  <p>
                    Des Pyrénées à Paris, en passant par le Mont Ventoux et
                    Strasbourg, "Au tour du vélo !" est une série de 12
                    émissions sous forme de documentaire. A bord d’une caravane
                    Airstream tirée par une voiture hybride, nos deux
                    animateurs, Gérard Holtz et Louise Ekland, vont partir sur
                    les routes de France à la rencontre des passionnés de vélo
                    pour comprendre, au travers de portraits et d’histoires
                    inédites...
                  </p>
                  <Link to="/emission">
                    <Button onClick={onClickSuite}>Lire la suite</Button>
                  </Link>
                  
                </div>
            </div>
          </div>
          <div className="row">
            <div
              className="col-md-offset-1 col-md-10 col-sm-12"
              style={{ marginTop: 25 }}
            />
          </div>
        </div>
        <div
          className="col-md-6 col-xs-12"
          style={{ paddingLeft: 0, paddingRight: 0 }}
        >
          <div className="container-concept">
            <PulseDiv className="item">
              <span>
                L'Heure des Pros
                <hr style={{ width: 60 }} />
              </span>
            </PulseDiv>
            <PulseDiv className="item">
              <span>
                Des Conseils Santé
                <hr style={{ width: 60 }} />
              </span>
            </PulseDiv>
            <PulseDiv className="item">
              <span>
                Innovation Vélo
                <hr style={{ width: 60 }} />
              </span>
            </PulseDiv>
            <PulseDiv className="item">
              <span>
                Personnalités mordues de Vélo
                <hr style={{ width: 60 }} />
              </span>
            </PulseDiv>
            <PulseDiv className="item">
              <span>
                L'histoire de Passionnés
                <hr style={{ width: 60 }} />
              </span>
            </PulseDiv>
          </div>
        </div>
      </div>
    </div>
    {/* Section l'équipe */}
    <div
      className="col-xs-12 presentateur"
      style={{ paddingLeft: 0, paddingRight: 0 }}
    >
      <div className="row">
        <Link to="/emission" className="col-xs-12" style={{textDecoration: "none"}}>
          <h2>Découvrez nos présentateurs</h2>
          <ChevronDown className="fas fa-chevron-down" />
        </Link>
        <Link to="/emission" className="col-md-6 col-xs-12 presentateur1" style={{textDecoration: "none"}}>
          <h3>Gérard Holtz</h3>
        </Link>
        <Link to="/emission" className="col-md-6 col-xs-12 presentateur2" style={{textDecoration: "none"}}>
          <h3>Louise Ekland</h3>
        </Link>
      </div>
    </div>
    <MapBoxSection />

    {/* Section partenaire */}
    <div
      className="col-xs-12 partenaire"
      style={{ textAlign: "center", background: "#fff" }}
    >
      <h2>Les Partenaires</h2>
      <div
        className="row middle-xs"
        style={{ marginTop: "70px", marginBottom: "90px" }}
      >
        <div className="col-xs-offset-2 col-xs-8">
          <div className="row middle-xs">
            <ImgContent className="col-md-6 col-xs-12 center-xs">
              <a href="http://www.volvo-iledefrance.com/" target="blank">
                <ImgPart src="img/ABVV.jpg" alt="photo" />
              </a>
            </ImgContent>

            <ImgContent className="col-md-6 col-xs-12">
              <a
                href="http://www.equipecycliste-groupama-fdj.fr"
                target="blank"
              >
                <ImgPart src="img/GROUPAMA-fdj.png" alt="photo" />
              </a>
            </ImgContent>
          </div>
        </div>
      </div>
    </div>
  </Style>
);

export default Home;
