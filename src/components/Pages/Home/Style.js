import styled from "styled-components";
import { colors } from "../../../utils/styles/helpers";
const { orange, grey, primary, white } = colors;
export default styled.div`
  h2 {
    font-family: 'Satisfy', cursive;
    font-weight: 400;
    font-size: 3rem;
    @media (max-width: 756px){
      font-size: 2.5rem;
    }
  }

  .paragraphe h2{
    background: url("img/banner-white.png") no-repeat center;
    background-size:500px ,250px;
    padding: 40px 0px;
    color: ${primary};
    @media (max-width: 756px){
      background-size: 100% 85%;
    }
  }

  h3 {
    font-family: 'Open Sans', sans-serif;
    font-weight: 500;
    font-size: 1.5rem;
    color: ${orange};
  }
  .paragraphe {
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    background: linear-gradient(rgba(84,159,141,0.8), rgba(42,102,84,0.8)), url("img/background-concept.png") no-repeat center;
    background-size: cover;
  }
  .paragraphe  p {
    font-family: 'Open Sans', sans-serif;
    font-weight: 500;
    font-style: italic;
    font-size: 1rem;
    text-align: center;
    letter-spacing: 0.1em;
    line-height: 1.5em;
    color: ${white};
    margin-bottom: 80px;
   }

   .biographie {
    font-size: 1rem;
    letter-spacing: normal;
    padding 0px 250px;
   }

   .container-concept {
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
     /* Shorthand – you could use ‘flex-direction: column’ and ‘flex-wrap: wrap’ instead */
    justify-content: flex-start;
    @media (max-width: 756px){
      flex-direction: row;
      height: auto;
    }
  }
  
  .item {
    display: flex;
    justify-content: center;
    align-items:center;
    background: linear-gradient(rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url("img/Pro.png") no-repeat center;
    background-size: cover;
    height: 50vh;
    @media (max-width: 756px){
      width: 100%;
    }
  }
  .item span{
    font-family: 'Open Sans', sans-serif;
    font-weight: 500;
    font-size: 1.25rem;
    opacity: 1;
    color: ${white};
  }
  
  .item:nth-child(2) {
    background: linear-gradient(rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url("img/Sante.png") no-repeat 50% 13%;
    background-size: cover;
    height: 50vh;
  }
  
  .item:nth-child(3) {
    background: linear-gradient(rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url("img/Bitmap.png") no-repeat center;
    background-size: cover;
    height: 20vh;
  }
  
  .item:nth-child(4) {
    background: linear-gradient(rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url("img/personnalité.jpg") center no-repeat;
    background-size: cover;
    height: 40vh;
  }
  .item:nth-child(5) {
    background: linear-gradient(rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url("img/passionné.jpeg") no-repeat;
    background-size: cover;
    height: 40vh;
  }

  .presentateur h3{
    background: url("img/banner-présentateur.png") no-repeat center;
    background-size:100% 85%;
    padding: 20px 150px;
  }
  
  .presentateur1 {
    display: flex;
    justify-content: center;
    background: linear-gradient(rgba(0,0,0,0.6), rgba(0,0,0,0.6)), url("img/presentateur1.png") no-repeat center;
    background-size: cover;
    height: 100vh;
    & > h3 {
      font-family: 'Satisfy', cursive;
      font-weight: 400;
      font-size: 3rem;
      color: ${white};
      align-self: flex-end;
      @media (max-width: 1174px){
        font-size: 2rem;
      }
      @media (max-width: 1007px){
        font-size: 2rem
        padding: 40px 100px;
      }
    }
  }

  .presentateur2 {
    display: flex;
    justify-content: center;
    background: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.4)), url("img/presentateur2.jpg") no-repeat center;
    background-size: cover;
    height: 100vh;
    & > h3 {
      font-family: 'Satisfy', cursive;
      font-weight: 400;
      font-size: 3rem;
      color: ${white};
      align-self: flex-end;
      @media (max-width: 1173px){
        font-size: 2rem;
      }
      @media (max-width: 1007px){
        padding: 40px 80px;
      }
    }
  }

  .presentateur {
    position: relative;
    & h2 {
      position: absolute;
      font-family: 'Open Sans', sans-serif;
      font-weight: 500;
      font-size: 1.25rem;
      width: auto;
      margin-left: auto;
      margin-right: auto;
      left: 0;
      right: 0;
      top: 20px;
      text-align:center;
      color: ${grey};
      z-index:3;
      @media (max-width: 767px){
        position: static;
        padding: 35px 0px;
      }
    }
  }
  .partenaire h2{
    background: url("img/banner-primary.png") no-repeat center;
    background-size:60% 85%;
    margin-top: 50px;
    padding: 40px 0px;
    color: ${white};
    @media (max-width: 1173px){
      font-size: 2rem;
    }
  }
      
`;
