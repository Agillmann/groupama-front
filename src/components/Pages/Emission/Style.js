import styled from "styled-components";
import { colors } from "../../../utils/styles/helpers";
const { orange, primary, white } = colors;
export default styled.div`
  h2 {
    font-family: 'Satisfy', cursive;
    font-weight: 400;
    font-size: 2rem;
  }

  .paragraphe h2{
    background: url("img/banner-white.png") no-repeat center;
    background-size:500px ,250px;
    margin-top: 50px;
    padding: 40px 0px;
    color: ${primary};
  }

  h3 {
    font-family: 'Open Sans', sans-serif;
    font-weight: 500;
    font-size: 1.5rem;
    color: ${orange};
  }
  .paragraphe {
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    height: 90vh;
    background: linear-gradient(rgba(84,159,141,0.8), rgba(42,102,84,0.8)), url("img/background-concept.png") no-repeat center;
    background-size: cover;
    @media (max-width: 756px){
      height: 100%!important;
    }
  }
  .paragraphe  p {
    font-family: 'Open Sans', sans-serif;
    font-weight: 500;
    font-style: italic;
    font-size: 1rem;
    text-align: center;
    letter-spacing: 0.1em;
    line-height: 1.4em;
    color: ${white};
    margin-bottom: 80px;
   }

   .biographie {
    font-size: 1rem;
    letter-spacing: normal;
    padding 0px 250px;
   }

   .container-concept {
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
     /* Shorthand – you could use ‘flex-direction: column’ and ‘flex-wrap: wrap’ instead */
    justify-content: flex-start;
  }
  .presentateur-row {
    background: url("img/background-presentateur.png") no-repeat center;
    background-size: cover;
  }
  .presentateur {
    background: linear-gradient(rgba(84,159,141,0.8), rgba(42,102,84,0.8));
    background-size: cover;
    padding: 10px 0px 40px 0px;
    }
    .presentateur  p {
      font-family: 'Open Sans', sans-serif;
      font-weight: 500;
      font-style: italic;
      font-size: 1rem;
      text-align: center;
      letter-spacing: 0.1em;
      line-height: 1.3em;
      color: ${white};
      margin-bottom: 0px;
     }

     .presentateur h2{
      background: url("img/banner-white.png") no-repeat center;
      background-size:500px ,250px;
      margin-top: 30px;
      margin-bottom: 40px;
      padding: 40px 0px;
      color: ${primary};
    }
  
  }
`;
