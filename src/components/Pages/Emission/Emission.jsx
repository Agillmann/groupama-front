import React from "react";
import Style from "./Style";
import { Link } from "react-router-dom";
// import Slider from "../../Sliders/Slider";
// import SliderVideo from "../../Sliders/SliderVideo";
// import SliderVideo3 from "../../Sliders/SliderVideo3";
import styled from "styled-components";
import Button from "../../Button/ButtonContainer";
import { colors } from "../../../utils/styles/helpers";
import Carousel2 from "../../Sliders/Carousel2";

const { white, orange } = colors;

const Biographie = styled.p`
  text-align: justify !important;
  line-height: 0.5em;
  margin-bottom:0px;
  & span {
    font-weight: 900;
  }
`;
const Img1 = styled.img`
  width: 100%;
`;

const Img2 = styled.img`
  width: 60%;
  margin-bottom: 20px;
  @media (max-width: 756px){
    width: 70%;
  heigth: 60%;
  }
  
`;
const TextPartenaire = styled.p`
  line-height: 0.5em;
  margin-bottom: 40px !important;
  padding-left: 35px!important;
  padding-right: 35px!important;
  @media (max-width: 756px){
    padding-left: 5px!important;
    padding-right: 5px!important;
  }
`;
const TitleG = styled.h2`
  color: ${white}!important;
  font-size: 2.5rem!important;
  background: none!important;
`
const Emission = () => (
  <Style className="row">
    <section
      className="col-xs-12"
      style={{
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 0,
        marginTop: 100
      }}
    >
      {/* <Slider dataJson="imageEmission.json" imgHeight="60vh" /> */}
      <Carousel2/>
    </section>
    {/* Section Concept */}
    <div
      className="col-xs-12"
      style={{
        textAlign: "center",
        height: "auto",
        paddingLeft: 0,
        paddingRight: 0
      }}
    >
      <div className="row">
        <div
          className="col-xs-12 paragraphe"
          style={{ paddingLeft: 0, paddingRight: 0 }}
        >
          <h2>Notre Concept</h2>
          <div className="row">
            <div className="col-md-offset-1 col-md-10">
              <div>
                <p
                  style={{
                    marginBottom: 40
                  }}
                >
                  Des Pyrénées à Paris, en passant par le Mont Ventoux et
                  Strasbourg, "Au tour du vélo !" est une série de 12 émissions
                  sous forme de documentaire. A bord d’une caravane tirée par
                  une voiture hybride, nos deux animateurs, Gérard Holtz et
                  Louise Ekland, vont partir sur les routes de France à la
                  rencontre des passionnés de vélo pour comprendre, au travers
                  de portraits et d’histoires inédites, ce qui fait de ce moyen
                  de transport centenaire l’objet d’une passion sans égal.
                  <br />
                  <br />
                  Ils recevront et mettront en avant aussi bien des compétiteurs
                  renommés que ceux qui vivent le vélo au quotidien, pour aller
                  travailler ou se détendre en famille. Les nouvelles tendances,
                  des conseils de professionnels, une rubrique santé seront
                  également au menu, avec des chroniques qui aborderont des
                  sujets tels que les nouveaux usages du vélo électrique, les
                  collectionneurs « vintage », les start-up innovantes…
                  L’occasion aussi de parler de l’engagement, de la
                  transmission, de la solidarité sans lesquels ce sport
                  n’existerait pas. Des valeurs chères à Groupama qui entend,
                  avec cette émission, donner la parole et mettre à l’honneur la
                  vraie vie de tous ces passionnés amoureux du vélo.
                </p>
                <p
                  style={{
                    textAlign: "right",
                    fontStyle: "italic",
                    fontSize: "1rem",
                    marginTop: 0
                  }}
                >
                  Au tour du vélo : une production Groupama & H.O.M.E
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div
              className="col-md-offset-1 col-md-10 col-sm-12"
              style={{ marginTop: 25 }}
            />
          </div>
        </div>
      </div>
    </div>
    {/* Section Presentateur */}
    <div
      className="col-xs-12"
      style={{
        textAlign: "center",
        height: "auto",
        paddingLeft: 0,
        paddingRight: 0
      }}
    >
      <div className="row presentateur-row">
        <div
          className="col-md-6 col-sm-12 presentateur"
          style={{ paddingLeft: 0, paddingRight: 0 }}
        >
          <h2>Nos présentateurs</h2>
          <div className="row middle-md" style={{marginBottom: 40}}>
            <div className="col-sm-4">
              <Img1 src="img/Louise.png" alt="photo Louis" />
            </div>
           
              <Biographie className="col-sm-8 middle-md">
                <span>Louise Ekland</span>
                <br />
                Journaliste et animatrice pour la télévision, Louise quitte
                l’Angleterre il y a 18 ans pour la France. Ancienne danseuse et
                supportrice du club de Liverpool, la plus frenchy des britishs
                aime le sport. C’est donc tout naturellement qu’elle animera des
                émissions sportives comme les JO 2012, l’Euro 2016 et Roland
                Garros pendant deux années consécutives.
              </Biographie>
         
          </div>
          <div className="row middle-md "style={{marginBottom: 40}}>
            <div className="col-sm-4">
              <Img1 src="img/Gerard.png" alt="photo Louis" />
            </div>
            <div className="col-sm-8">
              <Biographie>
                <span>Gérard Holtz</span>
                <br />
                Journaliste, commentateur sportif, animateur et écrivain. Avec
                44 ans de carrière, c’est une figure incontournable de la
                télévision française et du sport. C’est surtout la voix du Tour
                de France qu’il a animé pendant plus de 30 ans. En véritable
                passionné, il connait tous les plus grands cyclistes et les plus
                beaux circuits. Documentaires, livres, émissions, ... Gérard
                s'est donné une mission : Partager sa passion du vélo.
              </Biographie>
            </div>
          </div>
          <div className="col-xs-12">
            <Link to="/episodes">
              <Button
                color={white}
                background={orange}
                border="none"
                text="Découvrir les épisodes"
              />
            </Link>
          </div>
        </div>
      </div>
    </div>
    {/* Section Presentateur */}
    <div
      className="col-xs-12"
      style={{
        textAlign: "center",
        height: "auto",
        paddingLeft: 0,
        paddingRight: 0
      }}
    >
      <div className="row presentateur-row">
        <div
          className="col-md-6 col-sm-12 presentateur"
          style={{
            paddingLeft: 0,
            paddingRight: 0,
            background:
              "linear-gradient(rgba(84,159,141,0.8), rgba(42,102,84,0.8)), url('img/background-gpma1.png') no-repeat center",
            backgroundSize: "cover"
          }}
        >
          <TitleG>Groupama, un partenaire engagé...</TitleG>
          <div className="row">
            <TextPartenaire className="col-xs-12">
              Acteur majeur du cyclisme professionnel depuis la création de
              l'Equipe cycliste Groupama-FDJ en 2018, Groupama est fier de
              soutenir cette équipe toujours plus ambitieuse.
            </TextPartenaire>
            <TextPartenaire className="col-xs-12">
              Les nombreuses victoires remportées sont une belle illustration de
              la force de l'ADN mutualiste de Groupama qui place la confiance et
              le collectif au coeur de son action : c'est ensemble que l'on va
              plus loin !
            </TextPartenaire>
            <div className="col-xs-12">
              <Img2 src="img/Logo-FDJ.png" alt="logo fdj" />
            </div>
            <TextPartenaire className="col-xs-12">
              Inspiré par les défis que relèvent les champions de l'Equipe
              cycliste Groupama-FDJ, Groupama s'engage à promouvoir cette
              discipline populaire qu'est le vélo.
            </TextPartenaire>
          </div>
        </div>
        <div
          className="col-md-6 col-sm-12 presentateur"
          style={{
            paddingLeft: 0,
            paddingRight: 0,
            background:
              "linear-gradient(rgba(84,159,141,0.8), rgba(42,102,84,0.8)), url('img/background-gpma2.png') no-repeat center",
            backgroundSize: "cover"
          }}
        >
          <TitleG>... et en action</TitleG>
          <div className="row">
            <TextPartenaire className="col-xs-12">Se rendre au travail a vélo, faire des sorties sportives le week-end, se balader en famille, ... autant de pratiques diverses mais un même plaisir qui réunit quasi quotidiennement des millions de français partout en France.  
            </TextPartenaire>
            <TextPartenaire className="col-xs-12">
              Cette passion, Groupama s'est engagé à la valoriser dans le cadre de son programme "Nos engagements Vélo" au travers d'actions concrètes au service de ce sport accessible à tous. 
            </TextPartenaire>
            <TextPartenaire className="col-xs-12">Groupama entend s'adresser aux passionnés en venant à leur rencontre au sein de chacun de nos territoires et en s'impliquant là où ils aiment pratiquer ce sport.
            </TextPartenaire>
          </div>
          <div className="col-xs-12">
            <a href="https://www.groupama.fr/sponsoring-cyclisme" target="blank">
              <Button
                color={white}
                background={orange}
                border="none"
                text="Nos engagements Vélo"
              />
            </a>
          </div>
        </div>
      </div>
    </div>
    {/* background-concetp-em.png */}
  </Style>
);

export default Emission;
