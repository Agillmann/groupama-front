import React, { Component } from "react";
import { Form, Field } from "react-final-form";
import { Redirect } from "react-router";
import Helmet from "react-helmet";
import ErrorWithDelay from "./ErrorWithDelay";
import Style from "./Style";
import styled from "styled-components";
import { colors } from "../../../utils/styles/helpers";
import api from "../../../utils/api/api";

const { white } = colors;
const Title = styled.h1`
    font-family: 'Satisfy';
    font-weight: 500;
    font-size: 2.5em;
    color: ${props => props.color};
    background: url("${props => props.background}") no-repeat center;
    background-size:40% 90%;
    font-size: 2rem;
    text-align:center;
    padding: 50px 50px;
    margin-top: 170px;
    @media (max-width: 1023px){
        margin-top: 160px;
        background-size:75% 90%;
    }
  
`;
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formulaireOK: false,
      errorSubmit: []
    };
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  onSubmit = async values => {
    console.log("VALUES ONSUBMIT", values);

    const infoFormulaire = await {
      civilite: values.civilite,
      nom: values.nom.trim(),
      prenom: values.prenom.trim(),
      email: values.email,
      numero: values.numero,
      selectionType: values.selectionType,
      message: values.message.trim()
    };

    const resFormulaire = await api.postFormulaire(infoFormulaire);

    if (resFormulaire.statusCode === 400) {
      this.setState({ errorSubmit: resFormulaire });
      return resFormulaire.message;
    }

    if (resFormulaire) {
      console.log("Formulaire onsubmit --> ", resFormulaire);
      if (resFormulaire.email) {
        this.setState({ registerOK: true });
        // localStorage.setItem('client_conciergerie',JSON.stringify(resRegister));
        // localStorage.setItem('info_client',JSON.stringify(resInfoClient));
        return resFormulaire;
      }
      if (resFormulaire.statusCode === 400) {
        return resFormulaire.message;
      }
    }
  };

  //TEST REQUIRED, VALIDATE EMAIL
  required = value => (value ? undefined : "Champs requis");
  validateEmail = value => {
    var re = /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(String(value).toLowerCase())) {
      return "Mauvais format email";
    }
  };
  validateTelephone = value => {
    var re = /^(01|02|03|04|05|06|08)[0-9]{8}$/;
    if (!re.test(String(value).toLowerCase())) {
      return "Mauvais format telephone ex:0601020304";
    }
  };
  //
  composeValidators = (...validators) => value =>
    validators.reduce(
      (error, validator) => error || validator(value),
      undefined
    );

  render() {
    console.log(this.state);
    if (this.state.registerOK === true) {
      // localStorage.setItem('client_conciergerie',JSON.stringify(res.message));
      // c = localStorage.getItem('client_conciergerie')
      // console.log(JSON.parse(c))
      return <Redirect to="/" />;
    } else {
      return (
        <Style className="col-xs-12">
          <Helmet>
            <title>Formulaire de Contact</title>
            <meta name="keywords" content="HTML,CSS,JavaScript" />
            <meta name="author" content="Adrien Gillmann" />
            <meta name="description" content="Formulaire de contact" />
          </Helmet>
          <div className="col-xs-12 ">
            <Title background="img/banner-primary.png" color={white}>
              Formulaire de Contact
            </Title>
            <div className="col-sm-offset-2 col-sm-8 col-xs-12">
              <Form
                onSubmit={this.onSubmit}
                render={({
                  handleSubmit,
                  form,
                  submitting,
                  pristine,
                  values
                }) => (
                  <form onSubmit={handleSubmit}>
                    <div className="row">
                      <label className="col-xs-12">Civilité</label>
                      <div className="col-sm-3 col-xs-6">
                        <label>
                          <Field
                            name="civilite"
                            component="input"
                            type="radio"
                            value="Madame"
                            validate={this.required}
                          />{" "}
                          Madame
                        </label>
                      </div>
                      <div className="col-sm-3 col-xs-6">
                        <label>
                          <Field
                            name="civilite"
                            component="input"
                            type="radio"
                            value="Monsieur"
                            validate={this.required}
                          />{" "}
                          Monsieur
                        </label>
                      </div>
                      <ErrorWithDelay name="civilite" delay={8000}>
                        {error => <span>{error}</span>}
                      </ErrorWithDelay>
                    </div>
                    <div className="row">
                      <Field name="nom" validate={this.required}>
                        {({ input, meta }) => (
                          <div className="col-sm-6 col-xs-12">
                            <label>Nom</label>
                            <input {...input} type="text" placeholder="" />
                            <ErrorWithDelay name="nom" delay={8000}>
                              {error => <span>{error}</span>}
                            </ErrorWithDelay>
                          </div>
                        )}
                      </Field>
                      <Field name="prenom" validate={this.required}>
                        {({ input, meta }) => (
                          <div className="col-sm-6 col-xs-12">
                            <label>Prenom</label>
                            <input {...input} type="text" placeholder="" />
                            <ErrorWithDelay name="prenom" delay={8000}>
                              {error => <span>{error}</span>}
                            </ErrorWithDelay>
                          </div>
                        )}
                      </Field>
                      <Field
                        name="email"
                        validate={this.composeValidators(
                          this.required,
                          this.validateEmail
                        )}
                      >
                        {({ input, meta }) => (
                          <div className="col-xs-12">
                            <label>E-Mail</label>
                            <input {...input} type="email" placeholder="" />
                            <ErrorWithDelay name="email" delay={8000}>
                              {error => <span>{error}</span>}
                            </ErrorWithDelay>
                          </div>
                        )}
                      </Field>
                      <Field
                        name="numero"
                        validate={this.composeValidators(
                          this.required,
                          this.validateTelephone
                        )}
                      >
                        {({ input, meta }) => (
                          <div className="col-sm-12 col-xs-12">
                            <label>Telephone mobile</label>
                            <input {...input} type="text" placeholder="" />
                            <ErrorWithDelay name="numero" delay={8000}>
                              {error => <span>{error}</span>}
                            </ErrorWithDelay>
                          </div>
                        )}
                      </Field>
                    </div>
                    <Field name="selectionType">
                      {({ input, meta }) => (
                        <div className="col-xs-12">
                          <select {...input}>
                            <option value="Journaliste" selected>
                              Vous êtes Journaliste
                            </option>
                            <option value="Autre">Autre</option>
                          </select>
                          <ErrorWithDelay name="selectionType" delay={8000}>
                            {error => <span>{error}</span>}
                          </ErrorWithDelay>
                        </div>
                      )}
                    </Field>
                    <Field name="message" validate={this.required}>
                      {({ input, meta }) => (
                        <div className="col-xs-12">
                          <label>Message</label>
                          <textarea
                            {...input}
                            cols="50"
                            rows="5"
                            style={{ width: "100%" }}
                          />
                          <ErrorWithDelay name="message" delay={8000}>
                            {error => <span>{error}</span>}
                          </ErrorWithDelay>
                        </div>
                      )}
                    </Field>
                    <div className="row">
                      <div className="col-sm-offset-3 col-sm-6 col-xs-12 center-xs">
                        <button type="submit" disabled={submitting}>
                          <span>Envoyer</span>
                        </button>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-xs-12 center-xs">
                        <span>
                          {this.state.errorSubmit
                            ? this.state.errorSubmit.message
                            : []}
                        </span>
                      </div>
                    </div>
                    {/* <pre>{JSON.stringify(values, 0, 2)}</pre> */}
                  </form>
                )}
              />
            </div>
          </div>
        </Style>
      );
    }
  }
}

export default Register;
