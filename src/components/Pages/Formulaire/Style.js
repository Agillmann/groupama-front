import styled from "styled-components";
import { colors } from "../../../utils/styles/helpers";

const { black, primary, white } = colors;
export default styled.div`
    h2 {
      color: ${primary};
      font-family: 'Open Sans';
      font-weight: 300;
      font-size: 2.5em;
      margin: 0px 0px;
    }

    p {
      color: ${black};
      font-family: 'Open Sans';
      font-weight: 600;
      font-size: 1.2em;
      margin: 30px 0px;
    }
    label {
      display: block;
      color: ${black};
      font-family: 'Open Sans';
      font-weight: 300;
      font-size: 1em;
      margin: 10px 0px;
      & > [type=radio]{
        color: red;
      }
    }
    input {
      font-family: 'Open Sans';
      font-weight: 300;
      font-size: 1em;
      box-sizing: border-box;
      width: 100%;
      padding:10px 15px; 
      &[type='ratio']{
        display: inline-block;
      }
    }
    input[type=radio] {
      height:15px;
      width:40px;
    }
    select {
      font-family: 'Open Sans';
      font-weight: 300;
      font-size: 1em;
      padding:10px 15px; 
      margin-top: 25px;
      box-sizing: border-box;
      width: 90%;
      background: ${white};
      color: ${black}
    }
    span {
      display block;
      font-family: 'Open Sans';
      font-weight: 300;
      font-size: 0.80em;
      margin-top: 5px;
      color:red;
    }
    
    button {
      color: ${white};
      background: ${primary};
      border: none;
      border-radius: 34px;
      padding: 13px 23px;
      margin-top: 50px;
      margin-bottom: 80px;
      width: 70%; 
      transition: 0.33s;
      cursor:pointer;
      &:hover {
        position:relative;
        top: -2px;
      }
      & > span {
        font-family: 'Open Sans';
        font-weight: 700;
        letter-spacing: 0.1em;
        font-size: 1em;
        color: ${white};
      }
      
      textarea {
        box-sizing: border-box;
        padding:10px 15px; 
        display:block;/*reset from inline*/
        width:100%!important;
        overflow: auto;
        
      }
`;
