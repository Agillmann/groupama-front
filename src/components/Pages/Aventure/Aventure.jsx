import React from "react";
import Style from "./Style";
// import Slider from "../../Sliders/Slider";
import MapBoxSection from "../../Mapbox/MapboxContainer";
import styled from "styled-components";
import { colors } from "../../../utils/styles/helpers";
import Carousel3 from "../../Sliders/Carousel3";

const { white, primary, grey } = colors;
const TextPart = styled.h2`
  background: url("img/banner-primary.png") no-repeat center;
  background-size:60% 85%;
  margin-top: 50px;
  font-size: 2rem;
  padding: 40px 0px;
  color: ${white};
  @media (max-width: 1173px){
    font-size: 2rem!important;
  }
`;
const Text = styled.p`
  font-family: "Open Sans", sans-serif;
  font-weight: 500;
  font-size: 1rem;
  text-align: justify !important;
  color: ${white};
  padding-right: 150px;
  @media (max-width: 1023px) {
    padding: 0;
    text-align: center !important;
  }
`;
const Section = styled.section`
  padding-top: ${props => (props.paddingTop ? props.paddingTop : "30px")};
  background: ${props => props.background};
  background-size: ${props => props.backSize}
`;
const ChevronDown = styled.i`
  font-size: 2.5rem;
  color: ${grey};
  @media (max-width: 1023px) {
    display: none;
  }
`;
const IconRs = styled.i`
  font-size: 4em;
  color: ${white};
  margin: 0px 20px 0px 0px;
  @media (max-width: 756px) {
    font-size: 2em;
  }
  
`;
const ImgPart = styled.img`
  width: 300px;
  height: auto;
  @media (max-width: 756px) {
    width: 150px;
  }
`;
const ImgContent = styled.div`
  @media (max-width: 756px) {
    margin-bottom: 30px !important;
  }
`;
const AventureContent = styled.div`
  margin-bottom: 30px;
  position: relative;
  & h2 {
    position: absolute;
    font-family: "Satisfy", cursive;
    font-weight: 400;
    font-size: 3rem;
    width: auto;
    margin-left: auto;
    margin-right: auto;
    left: 0;
    right: 0;
    top: -160px;
    text-align: center;
    color: ${white};
    background: url("img/rectangleAventure.png") no-repeat center;
    padding: 100px;
    @media (max-width: 1023px) {
      background: url("img/banner-white.png") no-repeat center;
      background-size: 60% 50%;
      color: ${primary};
      position: static;
      padding: 60px;
    }
  }
`;
// const EquipeContent = styled.div`
// & img {
//   background: url("img/banner-white.png")no-repeat center;
//   background-size: 100% 100%;
//   @media (max-width: 1023px) {
//     max-width: 450px;
//   }

// `;
const Aventure = () => (
  <Style className="row">
    <section
      className="col-xs-12"
      style={{
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 0,
        marginTop: 100
      }}
    >
      {/* <Slider dataJson="image3Data.json" imgHeight="60vh" /> */}
      <Carousel3/>
    </section>
    <Section className="col-xs-12" background={primary}>
      <AventureContent className="row">
        <h2 className="col-xs-12">L'Aventure</h2>
        <div
          className="col-md-6 col-sm-12 center-xs"
          style={{ marginBottom: 0 }}
        >
          <div className="row">
            <div className="col-xs-12">
              <Text className="start-md center-xs">
                Suivez l'Aventure "Au tour du vélo" à travers nos Réseaux Sociaux
                et rejoignez-nous pour vivre une expérience unique aux côtés de
                Gérard Holtz et Louise Ekland.
              </Text>
            </div>
            <ChevronDown className="col-xs-12 fas fa-chevron-down" />
          </div>
        </div>
        <div className="col-md-6 col-sm-12">
          <div
            className="row middle-md"
            style={{ marginTop: "0.5em", marginnBottom: "0.5em" }}
          >
            <div className="col-xs-offset-2 col-xs-2 center-xs">
              <a
                href="https://www.facebook.com/groupama/"
                target="blank"
                style={{ textDecoration: "none" }}
              >
                <IconRs className="fab fa-facebook-square " />
              </a>
            </div>
            <div className="col-xs-2 center-xs">
              <a
                href="https://twitter.com/groupama"
                target="blank"
                style={{ textDecoration: "none" }}
              >
                <IconRs className="fab fa-twitter-square" />
              </a>
            </div>
            <div className="col-xs-2 center-xs">
              <a
                href="https://www.instagram.com/groupama"
                target="blank"
                style={{ textDecoration: "none" }}
              >
                <IconRs className="fab fa-instagram" />
              </a>
            </div>
            <div className="col-xs-2 center-xs">
              <a
                href="https://fr.linkedin.com/company/groupama"
                target="blank"
                style={{ textDecoration: "none" }}
              >
                <IconRs className="fab fa-linkedin" />
              </a>
            </div>
          </div>
        </div>
      </AventureContent>
    </Section>
    <Section
      className="col-xs-12"
      paddingTop="0"
      style={{ marginBottom: 30, background: "#A8A8A8" }}
    >
      <MapBoxSection />
    </Section>
    {/* <Section
      className="col-xs-12"
      background="url('img/Background-equipe.png') no-repeat center"
      backSize="100% 100%"
      style={{ height: "100vh" }}
    >
      <EquipeContent className="row">
        <div className="col-xs-12 center-xs">
          <img
            className=""
            src="img/GROUPAMA-fdj.png"
            alt="Logo groupama fdj"
            style={{ padding: "40px 60px" }}
          />
        </div>
      </EquipeContent>
    </Section> */}
    <Section
      className="col-xs-12 partenaire"
      style={{ textAlign: "center", background: "#fff" }}
    >
      <TextPart>Les Partenaires</TextPart>
      <div
        className="row middle-xs"
        style={{ marginTop: "80px", marginBottom: "90px" }}
      >
        <div className="col-xs-offset-2 col-xs-8">
          <div className="row middle-xs">
          <ImgContent className="col-md-6 col-xs-12">
              <a href="http://www.volvo-iledefrance.com/" target="blank"><ImgPart src="img/ABVV.jpg" alt="photo" /></a>
            </ImgContent>

            <ImgContent className="col-md-6 col-xs-12">
              <a href="http://www.equipecycliste-groupama-fdj.fr" target="blank"><ImgPart src="img/GROUPAMA-fdj.png" alt="photo" /></a>
            </ImgContent>
          </div>
        </div>
      </div>
    </Section>
  </Style>
);

export default Aventure;
