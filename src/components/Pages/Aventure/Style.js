import styled from "styled-components";
import { colors } from "../../../utils/styles/helpers";
const { orange } = colors;
export default styled.div`
  h2  {
    font-family: "Satisfy", cursive;
    font-weight: 400;
    font-size: 3rem;
  }

  h3 {
    font-family: "Open Sans", sans-serif;
    font-weight: 500;
    font-size: 1.5rem;
    color: ${orange};
  }
`;
