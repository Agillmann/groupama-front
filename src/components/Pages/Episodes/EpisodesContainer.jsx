import React, { Component } from "react";
import Episodes from "./Episodes";
import ReactGA from "react-ga";

class EpisodesContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      lireSuite: false
    };
  }
  componentDidMount() {
    window.scrollTo(0, 0);
    ReactGA.pageview(window.location.pathname);
  }
  onClickSuite = () => {
    this.setState({ lireSuite: true });
  };
  splitId = str => {
    return str.split("watch?v=")[1];
  };
  render() {
    //console.log(this.state.data);
    return (
      <div className="col-xs-12" style={{ paddingLeft: 0, paddingRight: 0 }}>
        <Episodes
        />
      </div>
    );
  }
}

export default EpisodesContainer;
