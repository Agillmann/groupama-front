import React from "react";
import Style from "./Style";
// import Slider from "../../Sliders/Slider";
// import SliderVideo from "../../Sliders/SliderVideo";
import SliderLast from "../../Sliders/SliderLast";
import SliderEpisode from "../../Sliders/SliderEpisode";
import SliderCapsule from "../../Sliders/SliderCapsule";
import SliderBest from "../../Sliders/SliderBest";
import styled from "styled-components";
import { colors } from "../../../utils/styles/helpers";
import Carousel4 from "../../Sliders/Carousel4";

const { white, primary } = colors;
const Title = styled.h2`
  display: block;
  background: url("${props => props.background}") no-repeat center;
  background-size:80% 100%;
  font-size: 2rem;
  padding: 50px 50px;
  color: ${props => props.color};
`;
const Section = styled.section`
  padding: 30px;
  background: url("${props => props.background}") no-repeat center;
`;
const Episodes = () => (
  <Style className="row">
    <section
      className="col-xs-12"
      style={{
        paddingLeft: 0,
        paddingRight: 0,
        marginBottom: 0,
        marginTop: 100
      }}
    >
      {/* <Slider dataJson="image2Data.json" imgHeight="60vh" /> */}
      <Carousel4/>
    </section>
    <Section className="col-xs-12">
      <div
        className="col-md-6 col-xs-12 center-xs"
        style={{ marginBottom: 40 }}
      >
        <Title background="img/banner-primary.png" color={white}>
          Les dernières vidéos
        </Title>
      </div>
      <div className="col-md-offset-2 col-md-8 col-xs-12">
        <SliderLast  />
        {/* <h3 className="center-xs" style={{fontSize: "3em",  margin:"100px 0px 100px 0px"}}>Vidéos bientôt disponibles</h3> */}
      </div>
    </Section>
    <Section className="col-xs-12" background="img/background-video.jpg">
      <div
        className="col-md-6 col-xs-12 center-xs"
        style={{ marginBottom: 40 }}
      >
        <Title background="img/banner-white.png" color={primary}>
          Les épisodes
        </Title>
      </div>
      <div className="col-md-offset-2 col-md-8 col-xs-12">
        <SliderEpisode/>
      </div>
    </Section>
    <Section className="col-xs-12">
      <div
        className="col-md-6 col-xs-12 center-xs"
        style={{ marginBottom: 40 }}
      >
        <Title background="img/banner-primary.png" color={white}>
          Les Capsules
        </Title>
      </div>
      <div className="col-md-offset-2 col-md-8 col-xs-12">
        <SliderCapsule/>
      </div>
    </Section>
    <Section className="col-xs-12" background="img/background-video.jpg">
      <div
        className="col-md-6 col-xs-12 center-xs"
        style={{ marginBottom: 40 }}
      >
        <Title background="img/banner-white.png" color={primary}>
          Les BestOf
        </Title>
      </div>
      <div className="col-md-offset-3 col-md-6 col-xs-12">
        <SliderBest/>
      </div>
    </Section>
  </Style>
);

export default Episodes;
