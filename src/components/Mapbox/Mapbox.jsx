import React from "react";
import Style from "./Style";
import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl";
import styled from "styled-components";
import { colors } from "../../utils/styles/helpers";
import Icon from "../../assets/icon/maps-and-flags.svg";
import CardLocation from "../Cards/CardLocation/CardLocationContainer";

import SlickSlider from "react-slick";

const Map = ReactMapboxGl({
  accessToken: process.env.REACT_APP_API_MAPBOX,
  doubleClickZoom: false,
  interactive: false,
  scrollZoom: true
});

const { grey } = colors;
const Text = styled.p`
  font-family: "Open-Sans" sans-serif;
  font-weight: 500;
  font-size: 1.2em;
  color: ${grey};
`;
const image = new Image(35, 35);
image.src = Icon;
const images = ["myImage", image];

const Mapbox = ({ center, changeCenter, zoom, interactive, settings, style }) => (
  <Style className="row">
    <div className="col-md-6 col-xs-12">
      <div className="row">
        <div className="col-xs-12">
          <div className="row" style={{ marginBottom: "40px" }}>
            <h2 className="col-xs-12 center-xs">Nous sommes sur la Route</h2>
            <Text className="col-xs-12 center-xs">
              Localiser les prochaines émissions en cliquant sur l’un des
              onglets ci-dessous{" "}
            </Text>
            <div className="col-xs-12">
              <SlickSlider {...settings}>
                <div onClick={() => changeCenter(0.157371, 43.055217, 13)}>
                  <CardLocation
                    de="de"
                    ville="Bagnères-de-Bigorre"
                    description="Le 5 Juin devant l'hôtel Carré Py à partir de 18H"
                    cardImg="img/Le-geant-du-Tourmalet_format_894x500.jpg"
                  />
                </div>
                <div onClick={() => changeCenter(-0.009352, 42.732204, 15)}>
                  <CardLocation
                    de="de"
                    ville="Gavarnie"
                    description="Le 6 Juin dans le village de Gavarnie à partir de 9H du matin"
                    cardImg="img/Gavarnie_recti_small_Wikimedia_Commons.jpg"
                  />
                </div>
                <div onClick={() => changeCenter(5.318994, 44.152341, 13)}>
                  <CardLocation
                    de="au"
                    ville="Chalet Reynard "
                    description="Le 12 Juin à 10H30 et 15H"
                    cardImg="img/Restaurant_du_Chalet_Reynard.jpg"
                  />
                </div>
                <div onClick={() => changeCenter(5.259401, 44.181045, 15)}>
                  <CardLocation
                    de="au"
                    ville="Chalet Liotard "
                    description="Le 13 Juin à 10H"
                    cardImg="img/Chalet_liotard.jpg"
                  />
                </div>
                <div onClick={() => changeCenter(7.74057, 48.5801, 15)}>
                  <CardLocation
                    de="de"
                    ville="Strasbourg "
                    description="le 17 Juin à partir de 10H30"
                    cardImg="img/strasbourg-img.jpg"
                  />
                </div>
                <div onClick={() => changeCenter(7.77647, 48.5903, 15)}>
                  <CardLocation
                    de="de"
                    ville="Strasbourg "
                    description="Le 18 Juin à partir de 10H30"
                    cardImg="img/strasbourg-img.jpg"
                  />
                </div>
                <div onClick={() => changeCenter(2.390213, 48.893898, 15)}>
                  <CardLocation
                    de="de"
                    ville="Paris "
                    description="Le 24 Juin Juin à 10H"
                    cardImg="img/paris-img.jpg"
                  />
                </div>
              </SlickSlider>
            </div>

            {/* <div className="col-md-6 col-xs-6 end-sm center-xs">
              <Button
                color={white}
                background={orange}
                border="none"
                text="Précédent"
              />
            </div>
            <div className="col-md-6 col-xs-6 start-sm center-xs">
              
            </div> */}
          </div>
        </div>
      </div>
    </div>
    <Map
      className="col-md-6 col-xs-12"
      style={style}
      containerStyle={{ height: "auto", width: "100vw", minHeight: "100vh" }}
      zoom={zoom}
      center={center}
      scrollZoom={false}
      interactive={interactive}
    >
      <Layer
        type="symbol"
        id="marker"
        layout={{ "icon-image": "myImage", "icon-allow-overlap": true }}
        images={images}
      >
        <Feature coordinates={[0.157371, 43.055217]} />
        <Feature coordinates={[-0.009352, 42.732204]} />
        <Feature coordinates={[5.318994, 44.152341]} />
        <Feature coordinates={[5.259401, 44.181045]} />
        <Feature coordinates={[7.74057, 48.5801]} />
        <Feature coordinates={[7.77647, 48.5903]} />
        <Feature coordinates={[2.390213, 48.893898]} />
      </Layer>
    </Map>
  </Style>
);

export default Mapbox;
