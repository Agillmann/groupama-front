import React, { Component } from "react";
import Mapbox from "./Mapbox";
import Button from "../Button/ButtonContainer"
import { colors } from "../../utils/styles/helpers";
const { orange, white } = colors;


// in render()

class MapboxContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      center: [1.7191036, 46.71109],
      zoom: [5],
      interactive: false,
      style : "mapbox://styles/mapbox/streets-v9",
      settings: {
        dots: false,
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplaySpeed: 4000,
        pauseOnHover: true,
        prevArrow: (
          <Button
            to="prev"
            color={white}
            background={orange}
            border="none"
            text="Précédent"
          />
        ),
        nextArrow: (
          <Button
            to="next"
            color={white}
            background={orange}
            border="none"
            text="Suivant"
          />
        ),
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          },
          {
            breakpoint: 765,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      }
    };
  }
  componentDidMount() {}
  handleClickCenter = (lng, lat, zoom) => {
    this.setState({ center: [lng, lat], zoom: [zoom] });
  };
  render() {
    return (
      <div className="col-md-12" style={{ background: "#EBEBEB" }}>
        <Mapbox
          center={this.state.center}
          changeCenter={this.handleClickCenter}
          zoom={this.state.zoom}
          interactive={false}
          settings={this.state.settings}
          style={this.state.style}
        />
      </div>
    );
  }
}

export default MapboxContainer;
