import styled from "styled-components";
import { colors } from "../../utils/styles/helpers";

const { white, grey } = colors;
export default styled.div`
  & h2 {
    background: url("img/banner-primary.png") no-repeat center;
    background-size:100% 85%;
    font-size: 2rem !important;
    padding: 50px 10px;
    color: ${white};
  }
  & h3 {
    font-family: "Open Sans", sans-serif;
    font-weight: 500;
    font-size: 1rem;
    color: ${grey};
    text-align: center;
    padding-left: 0.5rem;
    padding-right: 0.5rem;
  }
`;
