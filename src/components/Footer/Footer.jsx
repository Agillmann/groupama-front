import React from "react";
import { Link } from "react-router-dom";

import styled from "styled-components";
import { colors } from "../../utils/styles/helpers";

const { primary,grey } = colors;

const FooterContent = styled.footer`
  padding 20px 0;
  border-top: 0.25rem solid ${primary};
  z-index:2;
  transition: all 0.5s ease-out;
  // @media (max-width: 1023px) {
  //   display: none;
  // }
`;

const TextLink = styled.h2`
  color: ${grey};
  font-family: "Satisfy", cursive;
  font-weight: 400;
  font-size: 1.5rem;
  @media(max-width: 1325px){
    font-size: 1rem;
  }
  @media(max-width: 768px){
    font-size: 1.5rem;
    margin-bottom: 15px;
  }
`;

const Img = styled.img`
  max-width: 300px;
  height: auto;
  margin: 10px 50px 10px 0px;
  transition: all 0.5s ease-out;
  @media (max-width: 1354px) {
    max-width: 200px;
  }
  @media (max-width: 768px) {
    margin-left: 45px;
  }
`;

const IconRs = styled.i`
  font-size: 50px;
  color: ${primary};
  margin: 0px 20px 0px 0px;
  @media (max-width: 1354px) {
    font-size: 32px;
  }
`;

const Footer = () => (
  <FooterContent className="row middle-xs">
    <Link
      className="col-md-3 col-xs-12 center-xs"
      to="/"
      style={{ textDecoration: "none" }}
    >
      <Img className="logo" src="img/logo.jpeg" alt="Logo" />
    </Link>
    <a
      className="col-md-3 col-xs-12 center-xs"
      href="https://www.groupama.fr/mentions-legales"
      style={{ textDecoration: "none" }}
      target="blank"
    >
      <TextLink>Mentions Légales</TextLink>
    </a>
    <a
      className="col-md-3 col-xs-12 center-xs"
      href="https://www.groupama.fr/donnees-personnelles"
      style={{ textDecoration: "none" }}
      target="blank"
    >
      <TextLink>Données Personnelles </TextLink>
    </a>
    {/* <Link
      className="col-md-2 col-xs-12 center-xs"
      to="/plan-du-site"
      style={{ textDecoration: "none" }}
    >
      <TextLink>Plan du Site</TextLink>
    </Link> */}
    <div className="col-md-3 col-xs-12 center-xs">
      <a
        href="https://www.facebook.com/groupama/"
        target="blank"
        style={{ textDecoration: "none" }}
      >
        <IconRs className="fab fa-facebook-square " />
      </a>
      <a
        href="https://twitter.com/groupama"
        target="blank"
        style={{ textDecoration: "none" }}
      >
        <IconRs className="fab fa-twitter-square" />
      </a>
      <a
        href="https://www.instagram.com/groupama"
        target="blank"
        style={{ textDecoration: "none" }}
      >
        <IconRs className="fab fa-instagram" />
      </a>
      <a
        href="https://fr.linkedin.com/company/groupama"
        target="blank"
        style={{ textDecoration: "none" }}
      >
        <IconRs className="fab fa-linkedin" />
      </a>
    </div>
  </FooterContent>
);

export default Footer;
