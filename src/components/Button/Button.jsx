import React from "react";
import styled from "styled-components";

const ButtonStyle = styled.button`
  color: ${props => props.color};
  border-radius: 34px;
  padding: 13px 23px;
  min-width: 170px;
  border: ${props => props.border};
  background: ${props => props.background};
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15);
  transition: 0.33s;
  @media (max-width: 756px){
    padding: 13px 13px;
    min-width: 135px;
  }

  // &:after {
  //   box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15);
  // }

  // /* Transition to showing the bigger shadow on hover */
  // &:hover {
  //   box-shadow: 0 4px 9px rgba(0, 0, 0, 0.3);
  //   position: relative;
  //   top: -2px;
  // }
  box-shadow: 8px 10px 8px -9px #777;
  cursor: pointer;
`;

const TextButton = styled.span`
  font-family: "Open Sans", sans-serif;
  font-weight: 500;
  letter-spacing: 0.1em;
  font-size: 1em;
`;

const Button = ({ text, color, background, border, to, onClick}) => (
    <ButtonStyle color={color} background={background} border={border} aria-label={to} onClick={onClick} >
      <TextButton>{text}</TextButton>
    </ButtonStyle>
);

export default Button;
