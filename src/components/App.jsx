/* eslint-disable react/prefer-stateless-function */
import React, {Component}from "react";
import { Route, Router } from "react-router-dom";
import '../index.css';
import Home from "./Pages/Home/HomeContainer";
import Episodes from "./Pages/Episodes/EpisodesContainer";
import Aventure from "./Pages/Aventure/AventureContainer";
import Contact from "./Pages/Formulaire/Contact";
import Exemple from "./ModalYoutube/ModalYoutubeContainer";
import Emission from "./Pages/Emission/EmissionContainer";
import Header from './Header/Header';
import Footer from './Footer/Footer';
import createHistory from 'history/createBrowserHistory'

const history = createHistory()


class App extends Component {
  componentDidMount(){
    
  }
  render() {
    return (
      <Router history={history}>
      <div className="row">
        <div className="col-xs-12" style={{ paddingLeft: 0, paddingRight: 0 }}>
          <Header />
          <main className="row">
            
              <Route exact path="/" component={Home} />
              <Route exact path="/emission" component={Emission} />
              <Route path="/episodes" component={Episodes} />
              <Route path="/aventure" component={Aventure} />
              <Route path="/contact" component={Contact} />
              <Route path="/test" component={Exemple} />
          
          </main>
          <Footer />
        </div>
      </div>
      </Router>
    );
  }
}

export default App;
