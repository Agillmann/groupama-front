// // LOCAL API
// const VIDEO_API_URL = "http://localhost:1337/videos";
// const VIDEO_API_LAST = "http://localhost:1337/categories/5cb6f2ba88170a1284da3782";
// const VIDEO_API_EPISODE = "http://localhost:1337/categories/5cb6f2d688170a1284da3783";
// const VIDEO_API_CAPSULE = "http://localhost:1337/categories/5cb6f2e288170a1284da3784";
// const VIDEO_API_BEST = "http://localhost:1337/categories/5d0bc39513d87972c19876ea";
// const FORMULAIRE_URL = "http://localhost:1337/formulaires";

// // HEROKU HOST;
//const VIDEO_API_URL = "https://groupama-api.herokuapp.com/videos";

// // Groupama HOST;
const VIDEO_API_URL = "https://manage-autourduvelo.groupama.fr/videos";
const VIDEO_API_LAST = "https://manage-autourduvelo.groupama.fr/categories/5d0c289c758516105dd6f693";
const VIDEO_API_EPISODE = "https://manage-autourduvelo.groupama.fr/categories/5d0c28a4758516105dd6f694";
const VIDEO_API_CAPSULE = "https://manage-autourduvelo.groupama.fr/categories/5d0c28ad758516105dd6f695";
const VIDEO_API_BEST = "https://manage-autourduvelo.groupama.fr/categories/5d0c28b6758516105dd6f696";
const FORMULAIRE_URL = "https://manage-autourduvelo.groupama.fr/formulaires"


class api {
  // GET SERVICE PARENT
  async getVideoList() {
    const options = {
      method: "get",
      headers: {
        "Content-type": "application/json"
      }
    };
    const response = await fetch(VIDEO_API_URL, options);
    const json = await response.json();
    console.log(
      "Request Api : " + VIDEO_API_URL + " (getVideoList) ----------> : ",
      json
    );
    return json;
  }

  async getCapsuleVideo() {
    const options = {
      method: "get",
      headers: {
        "Content-type": "application/json"
      }
    };
    const response = await fetch(VIDEO_API_CAPSULE, options);
    const json = await response.json();
    console.log(
      "Request Api : " + VIDEO_API_CAPSULE + " (getCapsuleVideo) ----------> : ",
      json
    );
    return json;
  }

  async getBestVideo() {
    const options = {
      method: "get",
      headers: {
        "Content-type": "application/json"
      }
    };
    const response = await fetch(VIDEO_API_BEST, options);
    const json = await response.json();
    console.log(
      "Request Api : " + VIDEO_API_BEST + " (getBestVideo) ----------> : ",
      json
    );
    return json;
  }

  async getEpisodeVideo() {
    const options = {
      method: "get",
      headers: {
        "Content-type": "application/json"
      }
    };
    const response = await fetch(VIDEO_API_EPISODE, options);
    const json = await response.json();
    console.log(
      "Request Api : " + VIDEO_API_EPISODE + " (getEpisodeVideo) ----------> : ",
      json
    );
    return json;
  }

  async getLastVideo() {
    const options = {
      method: "get",
      headers: {
        "Content-type": "application/json"
      }
    };
    const response = await fetch(VIDEO_API_LAST, options);
    const json = await response.json();
    console.log(
      "Request Api : " + VIDEO_API_LAST + " (getLastVideo) ----------> : ",
      json
    );
    return json;
  }

  // POST Formulaire
  async postFormulaire(param) {
    let body = JSON.stringify(param);
    // console.log(typeof(body));
    // console.log(body);
    const options = {
      method: "post",
      headers: {
        "Content-type": "application/json"
      },
      body: body
    };
    let url = FORMULAIRE_URL;
    const response = await fetch(url, options);
    const json = await response.json();
    console.log("Request Api : " + FORMULAIRE_URL + " (postFormulaire) ----------> : ", json);
    return json;
  }

  // async postLogin(param) {
  //     let body = JSON.stringify(param);
  //     // console.log(typeof(body));
  //     // console.log(body);
  //     const options = {
  //         method: 'post',
  //         headers: {
  //             'Content-type': 'application/json',
  //         },
  //         body: body
  //     }
  //     let url = LOGIN_URL;
  //     const response = await fetch(url, options);
  //     const json = await response.json();
  //     console.log("api json Login : ", json);
  //     return json;
  // }

  // async getInfoById(id, token) {
  //     var bearer = 'Bearer ' + token;
  //     const options = {
  //         method: 'GET',
  //         withCredentials: true,
  //         headers: {
  //             'Authorization': bearer,
  //             'Content-Type': 'application/json',

  //         }
  //     }
  //     let url = INFOCLIENT_URL + id;
  //     const response = await fetch(url, options);
  //     const json = await response.json();
  //     console.log("api json InfoClient ---------->  ", json);
  //     return json;
  // }
}

export default new api();
