/**
 * Helper styles and themes for global usage
 */
export const colors = {
  primary: "#2A6654",
  white: "#F5F6FD",
  black: "#424242",
  orange: "#FF7B01",
  darkNavy: "#04264C",
  lightPurpleOne: "#A9A7EB",
  lightPurpleTwo: "#7D74AF",
  grayAlt: "#54575E",
  grey: "#A8A8A8",
  cloudGray: "#8D99AE",
  lightBlue: "#B1DDF1",
  shadows: {
    base: "0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)",
    light: "0px 0px 11px rgba(45, 45, 45, 0.11)"
  },
  gradient: {
    lightPurple:
      "linear-gradient(90deg, rgba(78,113,251,1) 0%, rgba(158,155,255,1) 100%)"
  }
};
